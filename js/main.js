$('#handle').on('touchstart', function(e) {
    $('body').css('overflow', 'hidden')
});

$('#handle').on('touchend', function(e) {
    $('body').css('overflow', 'visible')
});

$(".youtube").modalVideo({
    youtube: {
        controls: 1,
        nocookie: true
    }
});

var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("dot");
    if (n > slides.length) { slideIndex = 1 }
    if (n < 1) { slideIndex = slides.length }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " active";

    $('.s1').attr("src","img/bondi_button_unselect.png");
    $('.s2').attr("src","img/ruby_button_unselect.png");
    $('.s3').attr("src","img/graphite_button_unselect.png");
    $('.s4').attr("src","img/sage_button_unselect.png");
    $('.s5').attr("src","img/snow_button_unselect.png");

    if (n == 1) {
        $('.rainbow').css('background-color', '#5ac9c8');
        $('.s1').attr("src","img/bondi_button_select.png");
    }
    if (n == 2) {
        $('.rainbow').css('background-color', '#e28b8f');
        $('.s2').attr("src","img/ruby_button_select.png");
    }
    if (n == 3) {
        $('.rainbow').css('background-color', '#b8bcc8');
        $('.s3').attr("src","img/graphite_button_select.png");
    }
    if (n == 4) {
        $('.rainbow').css('background-color', '#65ad80');
        $('.s4').attr("src","img/sage_button_select.png");
    }
    if (n == 5) {
        $('.rainbow').css('background-color', '#5ac9c8');
        $('.s5').attr("src","img/snow_button_select.png");
    }
}

jQuery(document).ready(function($){
    
    ///////////////////
    // Points Effect //
    ///////////////////

    //open interest point description
	$('.spigen-single-point').children('a').hover(function(){
		var selectedPoint = $(this).parent('li');
		if( selectedPoint.hasClass('is-open') ) {
			selectedPoint.removeClass('is-open').addClass('visited');
		} else {
			selectedPoint.addClass('is-open').siblings('.spigen-single-point.is-open').removeClass('is-open').addClass('visited');
		}
    });
    
	//close interest point description
	$('.spigen-close-info').on('click', function(event){
		event.preventDefault();
		$(this).parents('.spigen-single-point').eq(0).removeClass('is-open').addClass('visited');
    });
    
    /////////////////////
    // Vertical Effect //
    /////////////////////

    var dragging = false,
        scrolling = false,
        resizing = false;
    //cache jQuery objects
    var imageComparisonContainers = $('.cd-image-container');
    //check if the .cd-image-container is in the viewport 
    //if yes, animate it
    checkPosition(imageComparisonContainers);
    $(window).on('scroll', function(){
        if( !scrolling) {
            scrolling =  true;
            ( !window.requestAnimationFrame )
                ? setTimeout(function(){checkPosition(imageComparisonContainers);}, 100)
                : requestAnimationFrame(function(){checkPosition(imageComparisonContainers);});
        }
    });
    
    //make the .cd-handle element draggable and modify .cd-resize-img width according to its position
    imageComparisonContainers.each(function(){
        var actual = $(this);
        drags(actual.find('.cd-handle'), actual.find('.cd-resize-img'), actual, actual.find('.cd-image-label[data-type="original"]'), actual.find('.cd-image-label[data-type="modified"]'));
    });

    //upadate images label visibility
    $(window).on('resize', function(){
        if( !resizing) {
            resizing =  true;
            ( !window.requestAnimationFrame )
                ? setTimeout(function(){checkLabel(imageComparisonContainers);}, 100)
                : requestAnimationFrame(function(){checkLabel(imageComparisonContainers);});
        }
    });

    function checkPosition(container) {
        container.each(function(){
            var actualContainer = $(this);
            if( $(window).scrollTop() + $(window).height()*0.5 > actualContainer.offset().top) {
                actualContainer.addClass('is-visible');
            } else {
                actualContainer.removeClass('is-visible');
            }
        });

        scrolling = false;
    }

    function checkLabel(container) {
        container.each(function(){
            var actual = $(this);
            updateLabel(actual.find('.cd-image-label[data-type="modified"]'), actual.find('.cd-resize-img'), 'left');
            updateLabel(actual.find('.cd-image-label[data-type="original"]'), actual.find('.cd-resize-img'), 'right');
        });

        resizing = false;
    }

    //draggable funtionality - credits to http://css-tricks.com/snippets/jquery/draggable-without-jquery-ui/
    function drags(dragElement, resizeElement, container, labelContainer, labelResizeElement) {
        dragElement.on("mousedown vmousedown", function(e) {
            dragElement.addClass('draggable');
            resizeElement.addClass('resizable');

            var dragWidth = dragElement.outerWidth(),
                xPosition = dragElement.offset().left + dragWidth - e.pageX,
                containerOffset = container.offset().left,
                containerWidth = container.outerWidth(),
                minLeft = containerOffset + 0,
                maxLeft = containerOffset + containerWidth - dragWidth - 0;

                console.log(dragWidth,xPosition,containerOffset, containerWidth,minLeft, maxLeft);
            dragElement.parents().on("mousemove vmousemove", function(e) {
                if( !dragging) {
                    dragging =  true;
                    ( !window.requestAnimationFrame )
                        ? setTimeout(function(){animateDraggedHandle(e, xPosition, dragWidth, minLeft, maxLeft, containerOffset, containerWidth, resizeElement, labelContainer, labelResizeElement);}, 100)
                        : requestAnimationFrame(function(){animateDraggedHandle(e, xPosition, dragWidth, minLeft, maxLeft, containerOffset, containerWidth, resizeElement, labelContainer, labelResizeElement);});
                }
            }).on("mouseup vmouseup", function(e){
                dragElement.removeClass('draggable');
                resizeElement.removeClass('resizable');
            });
            e.preventDefault();
        }).on("mouseup vmouseup", function(e) {
            dragElement.removeClass('draggable');
            resizeElement.removeClass('resizable');
        });
    }

    function animateDraggedHandle(e, xPosition, dragWidth, minLeft, maxLeft, containerOffset, containerWidth, resizeElement, labelContainer, labelResizeElement) {
        var leftValue = e.pageX + xPosition - dragWidth;   
        //constrain the draggable element to move inside his container
        if(leftValue < minLeft ) {
            leftValue = minLeft;
        } else if ( leftValue > maxLeft) {
            leftValue = maxLeft;
        }

        var widthValue = (leftValue + dragWidth/2 - containerOffset)*100/containerWidth+'%';
        
        $('.draggable').css('left', widthValue).on("mouseup vmouseup", function() {
            $(this).removeClass('draggable');
            resizeElement.removeClass('resizable');
        });

        $('.resizable').css('width', widthValue); 

        updateLabel(labelResizeElement, resizeElement, 'left');
        updateLabel(labelContainer, resizeElement, 'right');
        dragging =  false;
    }

    function updateLabel(label, resizeElement, position) {
        if(position == 'left') {
            ( label.offset().left + label.outerWidth() < resizeElement.offset().left + resizeElement.outerWidth() ) ? label.removeClass('is-hidden') : label.addClass('is-hidden') ;
        } else {
            ( label.offset().left > resizeElement.offset().left + resizeElement.outerWidth() ) ? label.removeClass('is-hidden') : label.addClass('is-hidden') ;
        }
    }
    
    
    //////////////////////
    // Horizontal Effect//
    //////////////////////

    var draggingHorizontal = false,
        scrollingHorizontal = false,
        resizingHorizontal = false;
    //cache jQuery objects
    var imageComparisonContainersHorizontal = $('.cd-image-container-horizontal');
    //check if the .cd-image-container is in the viewport 
    //if yes, animate it
    checkPositionHorizontal(imageComparisonContainersHorizontal);
    $(window).on('scroll', function(){
        if( !scrollingHorizontal) {
            scrollingHorizontal =  true;
            ( !window.requestAnimationFrame )
                ? setTimeout(function(){checkPositionHorizontal(imageComparisonContainersHorizontal);}, 100)
                : requestAnimationFrame(function(){checkPositionHorizontal(imageComparisonContainersHorizontal);});
        }
    });
    
    //make the .cd-handle element draggable and modify .cd-resize-img-horizontal width according to its position
    imageComparisonContainersHorizontal.each(function(){
        var actual = $(this);
        dragsHorizontal(actual.find('.cd-handle-horizontal'), actual.find('.cd-resize-img-horizontal'), actual, actual.find('.cd-image-label-horizontal[data-type="originalHorizontal"]'), actual.find('.cd-image-label-horizontal[data-type="modifiedHorizontal"]'));
    });

    //upadate images label visibility
    $(window).on('resize', function(){
        if( !resizingHorizontal) {
            resizingHorizontal =  true;
            ( !window.requestAnimationFrame )
                ? setTimeout(function(){checkLabelHorizontal(imageComparisonContainersHorizontal);}, 100)
                : requestAnimationFrame(function(){checkLabelHorizontal(imageComparisonContainersHorizontal);});
        }
    });

    function checkPositionHorizontal(container) {
        container.each(function(){
            var actualContainer = $(this);
            if( $(window).scrollTop() + $(window).height()*0.5 > actualContainer.offset().top) {
                actualContainer.addClass('is-visible-horizontal');
            } else {
                actualContainer.removeClass('is-visible-horizontal');
            }
        });

        scrollingHorizontal = false;
    }

    function checkLabelHorizontal(container) {
        container.each(function(){
            var actual = $(this);
            updateLabelHorizontal(actual.find('.cd-image-label-horizontal[data-type="modifiedHorizontal"]'), actual.find('.cd-resize-img-horizontal'), 'left');
            updateLabelHorizontal(actual.find('.cd-image-label-horizontal[data-type="originalHorizontal"]'), actual.find('.cd-resize-img-horizontal'), 'right');
        });

        resizingHorizontal = false;
    }

    //draggable funtionality - credits to http://css-tricks.com/snippets/jquery/draggable-without-jquery-ui/
    function dragsHorizontal(dragElement, resizeElement, container, labelContainer, labelResizeElement) {
        dragElement.on("mousedown vmousedown", function(e) {
            dragElement.addClass('draggableHorizontal');
            resizeElement.addClass('resizableHorizontal');

            var dragWidth = dragElement.outerHeight(),
                xPosition = dragElement.offset().top + dragWidth - e.pageY,
                containerOffset = container.offset().top,
                containerWidth = container.outerHeight(),
                minLeft = containerOffset + 10,
                maxLeft = containerOffset + containerWidth - dragWidth - 10;
            
            dragElement.parents().on("mousemove vmousemove", function(e) {
                if( !draggingHorizontal) {
                    draggingHorizontal =  true;
                    ( !window.requestAnimationFrame )
                        ? setTimeout(function(){animateDraggedHandleHorizontal(e, xPosition, dragWidth, minLeft, maxLeft, containerOffset, containerWidth, resizeElement, labelContainer, labelResizeElement);}, 100)
                        : requestAnimationFrame(function(){animateDraggedHandleHorizontal(e, xPosition, dragWidth, minLeft, maxLeft, containerOffset, containerWidth, resizeElement, labelContainer, labelResizeElement);});
                }
            }).on("mouseup vmouseup", function(e){
                dragElement.removeClass('draggableHorizontal');
                resizeElement.removeClass('resizableHorizontal');
            });
            e.preventDefault();
        }).on("mouseup vmouseup", function(e) {
            dragElement.removeClass('draggableHorizontal');
            resizeElement.removeClass('resizableHorizontal');
        });
    }

    function animateDraggedHandleHorizontal(e, xPosition, dragWidth, minLeft, maxLeft, containerOffset, containerWidth, resizeElement, labelContainer, labelResizeElement) {
        var leftValue = e.pageY + xPosition - dragWidth;   
        //constrain the draggable element to move inside his container
        if(leftValue < minLeft ) {
            leftValue = minLeft;
        } else if ( leftValue > maxLeft) {
            leftValue = maxLeft;
        }

        var widthValue = (leftValue + dragWidth/2 - containerOffset)*100/containerWidth+'%';
        
        $('.draggableHorizontal').css('top', widthValue).on("mouseup vmouseup", function() {
            $(this).removeClass('draggableHorizontal');
            resizeElement.removeClass('resizableHorizontal');
        });

        $('.resizableHorizontal').css('height', widthValue); 

        updateLabelHorizontal(labelResizeElement, resizeElement, 'top');
        updateLabelHorizontal(labelContainer, resizeElement, 'bottom');
        draggingHorizontal =  false;
    }

    function updateLabelHorizontal(label, resizeElement, position) {
        if(position == 'top') {
            ( label.offset().left + label.outerHeight() < resizeElement.offset().left + resizeElement.outerHeight() ) ? label.removeClass('is-hidden') : label.addClass('is-hidden') ;
        } else {
            ( label.offset().left > resizeElement.offset().left + resizeElement.outerHeight() ) ? label.removeClass('is-hidden') : label.addClass('is-hidden') ;
        }
    }

});
